package uk.tw.energy.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import uk.tw.energy.httpReponse.model.HttpErrorCode;
import uk.tw.energy.httpReponse.model.HttpReponseMessage;
import uk.tw.energy.httpReponse.model.HttpStatusCode;
import uk.tw.energy.httpReponse.model.HttpReponse;

@Service
public class CommonService {

	public HttpReponse prepareResponse(HttpStatus status,String httpReponseMessage, Long httpErrorCode, Object obj) {
   	 HttpReponse response = new HttpReponse();
   	 response.setData(obj);
   	 response.setHttpErrorCode(new HttpErrorCode(httpErrorCode));
   	 response.setHttpReponseMessage(new HttpReponseMessage(httpReponseMessage));
   	 response.setHttpStatusCode(new HttpStatusCode(status.value()));
   	 return response;
   	 
   }
}
