package uk.tw.energy.httpReponse.model;

public class HttpReponseMessage {

	private String httpReponseMessage;
	public HttpReponseMessage(String httpReponseMessage ) {
		this.httpReponseMessage = httpReponseMessage;
	}

	public final String getHttpReponseMessage() {
		return httpReponseMessage;
	}

	public final void setHttpReponseMessage(String httpReponseMessage) {
		this.httpReponseMessage = httpReponseMessage;
	}
	
}
