package uk.tw.energy.httpReponse.model;

public class HttpReponse {

	private HttpStatusCode httpStatusCode;

	private HttpReponseMessage httpReponseMessage;

	private HttpErrorCode httpErrorCode;

	private Object data;

	public final HttpStatusCode getHttpStatusCode() {
		return httpStatusCode;
	}

	public final void setHttpStatusCode(HttpStatusCode httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public final HttpReponseMessage getHttpReponseMessage() {
		return httpReponseMessage;
	}

	public final void setHttpReponseMessage(HttpReponseMessage httpReponseMessage) {
		this.httpReponseMessage = httpReponseMessage;
	}

	public final HttpErrorCode getHttpErrorCode() {
		return httpErrorCode;
	}

	public final void setHttpErrorCode(HttpErrorCode httpErrorCode) {
		this.httpErrorCode = httpErrorCode;
	}

	public final Object getData() {
		return data;
	}

	public final void setData(Object data) {
		this.data = data;
	}

}
