package uk.tw.energy.httpReponse.model;

public class HttpErrorCode {
	private Long errorCode;
	
	public HttpErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}
	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}
}
