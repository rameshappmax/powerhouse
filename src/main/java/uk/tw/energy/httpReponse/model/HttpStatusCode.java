package uk.tw.energy.httpReponse.model;

public class HttpStatusCode {

	private int httpStatusCode;

	public HttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	public final int getHttpStatusCode() {
		return httpStatusCode;
	}

	public final void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}
	
}
