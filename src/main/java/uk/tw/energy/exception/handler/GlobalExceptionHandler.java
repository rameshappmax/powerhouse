package uk.tw.energy.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import uk.tw.energy.httpReponse.model.HttpReponse;
import uk.tw.energy.service.CommonService;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@Autowired
	private CommonService commonservice;

	@ExceptionHandler(value = Exception.class)
    public HttpReponse excpetion(Exception exception) {
        return commonservice.prepareResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong", null, null);
    }
}
