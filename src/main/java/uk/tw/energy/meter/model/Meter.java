package uk.tw.energy.meter.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Meter {

	private String smartMeterId;

	private List<ElectricityReading> electricityReadings;

	@Autowired
	private Map<String, List<ElectricityReading>> meterAssociatedReadings;

	public Meter() {
	}
	public Meter(String smartMeterId, List<ElectricityReading> electricityReadings) {
		super();
		this.smartMeterId = smartMeterId;
		this.electricityReadings = electricityReadings;
	}
	public List<ElectricityReading> getElectricityReadings() {
		return electricityReadings;
	}

	public String getSmartMeterId() {
		return smartMeterId;
	}

	public Optional<List<ElectricityReading>> getReadings(String smartMeterId) {
		return Optional.ofNullable(meterAssociatedReadings.get(smartMeterId));
	}

	public void storeReadings(Meter electricityReadings) {
		if (!meterAssociatedReadings.containsKey(electricityReadings.getSmartMeterId())) {
			meterAssociatedReadings.put(electricityReadings.getSmartMeterId(), new ArrayList<>());
		}
		meterAssociatedReadings.get(electricityReadings.getSmartMeterId()).addAll(electricityReadings.getElectricityReadings());
	}
}
