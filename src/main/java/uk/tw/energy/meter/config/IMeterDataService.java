package uk.tw.energy.meter.config;

import java.util.List;

public interface IMeterDataService {
	
	List<MeterConfig> smartMeterToPricePlanAccounts() throws Exception;
}
