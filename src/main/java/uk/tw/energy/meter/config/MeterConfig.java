package uk.tw.energy.meter.config;

public class MeterConfig {

	private String meterName;

	private String planName;

	public MeterConfig() {

	}

	public String getMeterName() {
		return meterName;
	}

	public void setMeterName(String meterName) {
		this.meterName = meterName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

}
