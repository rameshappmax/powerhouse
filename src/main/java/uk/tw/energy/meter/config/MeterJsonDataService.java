package uk.tw.energy.meter.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.tw.energy.meter.model.ElectricityReading;

@Configuration
public class MeterJsonDataService implements IMeterDataService { 
	
	@Autowired
	private ElectricityReadingsGenerator electricityReadingsGenerator;
	
    @Bean
    public List<MeterConfig> smartMeterToPricePlanAccounts() throws Exception {
    	return (List<MeterConfig>) new ObjectMapper()
    			.readValue(new ClassPathResource("meterConfig.json").getFile(),new TypeReference<List<MeterConfig>>(){});
    }
    
    @Bean
    public Map<String, List<ElectricityReading>> perMeterElectricityReadings() throws Exception {
        final Map<String, List<ElectricityReading>> readings = new HashMap<>();
        smartMeterToPricePlanAccounts()
                .forEach(smartMeter -> readings.put(smartMeter.getMeterName(), electricityReadingsGenerator.generate(20)));
        return readings;
    }
   
}
