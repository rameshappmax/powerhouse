package uk.tw.energy.meter.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uk.tw.energy.httpReponse.model.HttpReponse;
import uk.tw.energy.meter.model.ElectricityReading;
import uk.tw.energy.meter.model.Meter;
import uk.tw.energy.service.CommonService;

@RestController
@RequestMapping("/readings")
public class MeterReadingController {

	private CommonService commonService;
	
	private Meter  meter;

	public MeterReadingController(CommonService commonService,Meter meter) {
		this.meter = meter;
		this.commonService = commonService;
	}

	@PostMapping("/store")
	public HttpReponse storeReadings(@RequestBody Meter meterReadings) {
		if (!isMeterReadingsValid(meterReadings)) {
			return commonService.prepareResponse(HttpStatus.BAD_REQUEST, "INVALID DATA", null, null);
		}
		meter.storeReadings(meterReadings);
		return commonService.prepareResponse(HttpStatus.CREATED, "Meter Data Pushed", null, null);
	}

	@GetMapping("/read/{smartMeterId}")
	public HttpReponse readReadings(@PathVariable String smartMeterId) {
		Optional<List<ElectricityReading>> readings = meter.getReadings(smartMeterId);
		return readings.isPresent()
				? commonService.prepareResponse(HttpStatus.OK, "Meter Reading", null, readings.get())
				: commonService.prepareResponse(HttpStatus.NOT_FOUND, "Meter not Found", null, null);
	}

	private boolean isMeterReadingsValid(Meter meterReadings) {
		String smartMeterId = meterReadings.getSmartMeterId();
		List<ElectricityReading> electricityReadings = meterReadings.getElectricityReadings();
		return smartMeterId != null && !smartMeterId.isEmpty() && electricityReadings != null
				&& !electricityReadings.isEmpty();
	}
}
