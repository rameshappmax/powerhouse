package uk.tw.energy.plan.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class PricePlanJsonDataService implements IPricePlanDataService { 
	
	@Bean
    public List<PlanPriceCofig> loadPricePlans() throws Exception {
		List<PlanPriceCofig> pricePlans = new ArrayList<>();
			pricePlans = (List<PlanPriceCofig>) new ObjectMapper()
    			.readValue(new ClassPathResource("pricePlan.json").getFile(),new TypeReference<List<PlanPriceCofig>>(){});
        return pricePlans;
    }
  
}
