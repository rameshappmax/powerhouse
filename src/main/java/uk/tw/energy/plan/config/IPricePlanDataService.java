package uk.tw.energy.plan.config;

import java.util.List;

public interface IPricePlanDataService {
	
	List<PlanPriceCofig> loadPricePlans() throws Exception;

}
