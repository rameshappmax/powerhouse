package uk.tw.energy.plan.config;

import java.math.BigDecimal;

public class PlanPriceCofig {

	private String energySupplier;

	private String planName;

	private BigDecimal unitRate; // unit price per kWh

	public String getEnergySupplier() {
		return energySupplier;
	}

	public void setEnergySupplier(String energySupplier) {
		this.energySupplier = energySupplier;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public BigDecimal getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(BigDecimal unitRate) {
		this.unitRate = unitRate;
	}
	
}
