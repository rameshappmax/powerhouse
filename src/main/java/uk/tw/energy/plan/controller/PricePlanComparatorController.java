package uk.tw.energy.plan.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import uk.tw.energy.httpReponse.model.HttpReponse;
import uk.tw.energy.plan.services.AccountService;
import uk.tw.energy.plan.services.PricePlanService;
import uk.tw.energy.service.CommonService;

@RestController
@RequestMapping("/price-plans")
public class PricePlanComparatorController {

    public final static String PRICE_PLAN_ID_KEY = "pricePlanId";
    public final static String PRICE_PLAN_COMPARISONS_KEY = "pricePlanComparisons";
    private final PricePlanService pricePlanService;
    private final AccountService accountService;
    private final CommonService commonService;

    public PricePlanComparatorController(PricePlanService pricePlanService, 
    		AccountService accountService, CommonService commonService) {
        this.pricePlanService = pricePlanService;
        this.accountService = accountService;
        this.commonService = commonService;
    }

    @GetMapping("/compare-all/{smartMeterId}")
    public HttpReponse calculatedCostForEachPricePlan(@PathVariable String smartMeterId) {
        String pricePlanId = accountService.getPricePlanIdForSmartMeterId(smartMeterId);
        
        Optional<Map<String, BigDecimal>> consumptionsForPricePlans =
                pricePlanService.getConsumptionCostOfElectricityReadingsForEachPricePlan(smartMeterId);

        if (!consumptionsForPricePlans.isPresent()) {
            return this.commonService.prepareResponse(HttpStatus.NOT_FOUND, "Not Available", null, null);
        }

        Map<String, Object> pricePlanComparisons = new HashMap<>();
        pricePlanComparisons.put(PRICE_PLAN_ID_KEY, pricePlanId);
        pricePlanComparisons.put(PRICE_PLAN_COMPARISONS_KEY, consumptionsForPricePlans.get());

        return this.commonService.prepareResponse(HttpStatus.OK, "Plan Comparsion", null, pricePlanComparisons);
    }

    @GetMapping("/recommend/{smartMeterId}")
    public HttpReponse recommendCheapestPricePlans(@PathVariable String smartMeterId,
            @RequestParam(value = "limit", required = false) Integer limit) {
        Optional<Map<String, BigDecimal>> consumptionsForPricePlans =
                pricePlanService.getConsumptionCostOfElectricityReadingsForEachPricePlan(smartMeterId);

        if (!consumptionsForPricePlans.isPresent()) {
            return this.commonService.prepareResponse(HttpStatus.NOT_FOUND, "Not Available", null, null);
        }

        List<Map.Entry<String, BigDecimal>> recommendations = new ArrayList<>(consumptionsForPricePlans.get().entrySet());
        recommendations.sort(Comparator.comparing(Map.Entry::getValue));

        if (limit != null && limit < recommendations.size()) {
            recommendations = recommendations.subList(0, limit);
        }

        return this.commonService.prepareResponse(HttpStatus.OK, "Recommanded Plan", null, recommendations);
    }
}
