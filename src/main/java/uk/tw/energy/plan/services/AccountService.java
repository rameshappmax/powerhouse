package uk.tw.energy.plan.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uk.tw.energy.meter.config.MeterConfig;

@Service
public class AccountService {

	@Autowired
    private List<MeterConfig> smartMeterToPricePlanAccounts;

    public String getPricePlanIdForSmartMeterId(String smartMeterId) {
        return smartMeterToPricePlanAccounts.stream().filter( t -> t.getMeterName().equals(smartMeterId)).findFirst().get().getMeterName();
    }
}
