package uk.tw.energy.plan.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uk.tw.energy.meter.model.ElectricityReading;
import uk.tw.energy.meter.model.Meter;
import uk.tw.energy.plan.config.PlanPriceCofig;
import uk.tw.energy.plan.model.PlanPrice;

@Service
public class PricePlanService {

	@Autowired
	private  List<PlanPriceCofig>  pricePlans;

	private  Meter meter;
	
	private  PlanPrice planPrice;
	
	public PricePlanService(Meter meter,PlanPrice planPrice) {
		this.meter = meter;
		this.planPrice = planPrice;
	}
	
	public Optional<Map<String, BigDecimal>> getConsumptionCostOfElectricityReadingsForEachPricePlan(
			String smartMeterId) {
		Optional<List<ElectricityReading>> electricityReadings = meter.getReadings(smartMeterId);

		if (!electricityReadings.isPresent()) {
			return Optional.empty();
		}
		
 		return Optional.of(pricePlans.stream().collect(
				Collectors.toMap(PlanPriceCofig::getPlanName, t -> planPrice.calculateCost(electricityReadings.get(), t))));
	}
}
