package uk.tw.energy.plan.model;

import uk.tw.energy.meter.model.Meter;

public class Consumer {
	
	private int id;
	
	private String consumerName;
	
	private  Meter meter;
	
	private PlanPrice plan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public Meter getMeter() {
		return meter;
	}

	public void setMeter(Meter meter) {
		this.meter = meter;
	}

	public PlanPrice getPlan() {
		return plan;
	}

	public void setPlan(PlanPrice plan) {
		this.plan = plan;
	}
	
	
	 	 	
}
