package uk.tw.energy.plan.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class EngerySupplier {

	private int id;

	private String supplierName;

	private String supplierAddress;

	private List<PlanPrice> plans;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierAddress() {
		return supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public List<PlanPrice> getPlans() {
		return plans;
	}

	public void setPlans(List<PlanPrice> plans) {
		this.plans = plans;
	}

}
