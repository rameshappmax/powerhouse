package uk.tw.energy.plan.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

import uk.tw.energy.meter.model.ElectricityReading;
import uk.tw.energy.plan.config.PlanPriceCofig;

@Component
public class PlanPrice {

	private String energySupplier;

	private String planName;

	private BigDecimal unitRate; // unit price per kWh

	private List<PeakTimeMultiplier> peakTimeMultipliers;
	
	public PlanPrice(String planName,String energySupplier, BigDecimal unitRate,List<PeakTimeMultiplier> peakTimeMultipliers ) {
		this.energySupplier = energySupplier;
		this.planName = planName;
		this.unitRate= unitRate;
		this.peakTimeMultipliers = peakTimeMultipliers;
	}
	public PlanPrice() {
		// TODO Auto-generated constructor stub
	}

	public String getEnergySupplier() {
		return energySupplier;
	}

	public void setEnergySupplier(String energySupplier) {
		this.energySupplier = energySupplier;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public BigDecimal getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(BigDecimal unitRate) {
		this.unitRate = unitRate;
	}

	public List<PeakTimeMultiplier> getPeakTimeMultipliers() {
		return peakTimeMultipliers;
	}

	public void setPeakTimeMultipliers(List<PeakTimeMultiplier> peakTimeMultipliers) {
		this.peakTimeMultipliers = peakTimeMultipliers;
	}

	public BigDecimal getPrice(LocalDateTime dateTime) {
		return peakTimeMultipliers.stream().filter(multiplier -> multiplier.dayOfWeek.equals(dateTime.getDayOfWeek()))
				.findFirst().map(multiplier -> unitRate.multiply(multiplier.multiplier)).orElse(unitRate);
	}

	public BigDecimal calculateCost(List<ElectricityReading> electricityReadings, PlanPriceCofig planPrice) {
		BigDecimal average = calculateAverageReading(electricityReadings);
		BigDecimal timeElapsed = calculateTimeElapsed(electricityReadings);

		BigDecimal averagedCost = average.divide(timeElapsed, RoundingMode.HALF_UP);
		return averagedCost.multiply(planPrice.getUnitRate());
	}

	private BigDecimal calculateAverageReading(List<ElectricityReading> electricityReadings) {
		BigDecimal summedReadings = electricityReadings.stream().map(ElectricityReading::getReading)
				.reduce(BigDecimal.ZERO, (reading, accumulator) -> reading.add(accumulator));

		return summedReadings.divide(BigDecimal.valueOf(electricityReadings.size()), RoundingMode.HALF_UP);
	}

	private BigDecimal calculateTimeElapsed(List<ElectricityReading> electricityReadings) {
		ElectricityReading first = electricityReadings.stream().min(Comparator.comparing(ElectricityReading::getTime))
				.get();
		ElectricityReading last = electricityReadings.stream().max(Comparator.comparing(ElectricityReading::getTime))
				.get();

		return BigDecimal.valueOf(Duration.between(first.getTime(), last.getTime()).getSeconds() / 3600.0);
	}
}
